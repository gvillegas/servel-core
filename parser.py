# -*- coding: utf-8 -*-
# coding: utf8
import glob
import re

comunas = ["null", "NO DATA"]
provincias = ["null", "NO DATA"]
regiones = ["null", "NO DATA"]
gender = ["null", "NO DATA", "VAR", "MUJ"]


def search_nombre(lista, rut):
    posicion = lista.index(rut)
    a = 0
    list_tmp = []
    while a < posicion:
        list_tmp.append(lista[a])
        a += 1
    nombre_s = " ".join(list_tmp)
    return nombre_s


def search_genero(lista, rut):
    posicion = lista.index(rut)
    a = posicion + 1
    return lista[a]


def sanitize_mesa(lista):
    mesa_aux = re.search(r"([1-9][A-Z])", lista[-1])
    if mesa_aux:
        del lista[-1]
    return lista


def search_comuna(lista):
    comuna = "COMUNA:"
    pagina = "PAGINA"
    posicion = lista.index(comuna) + 1
    limite = lista.index(pagina)
    list_tmp = []
    while posicion < limite:
        list_tmp.append(lista[posicion])
        posicion += 1
    comuna_s = " ".join(list_tmp)
    return comuna_s


def search_region(lista):
    comuna = "COMUNA:"
    styck = ":"
    posicion = lista.index(styck) + 1
    limite = lista.index(comuna)
    list_tmp = []
    while posicion < limite:
        list_tmp.append(lista[posicion])
        posicion += 1
    region_s = " ".join(list_tmp)
    return region_s


def search_provincia(lista):
    list_tmp = []
    start = 2
    while start < len(lista):
        list_tmp.append(lista[start])
        start += 1
    provincia_s = " ".join(list_tmp)
    return provincia_s


def search_circuns_direccion(line, genero):
    data = {}
    fin_dir = 0
    count_blank = 0
    flag_blank = True
    for x in range(line.index(" " + genero + " ")+4, len(line), 1):
        if flag_blank and line[x].strip():
            flag_blank = False

        if not flag_blank and not line[x].strip():
            count_blank += 1
            if count_blank >= 2:
                fin_dir = x
                break

        if line[x].strip():
            count_blank = 0

    direccion = line[line.index(" " + genero + " ")+4:fin_dir].strip()
    mesa = line[line.__len__() - 10:].strip()
    mesa_match = re.search(r"([1-9]{1,2}[\s-]?[A-Z]?)", mesa)
    if mesa_match:
        circunscrip = line[fin_dir+1:line.__len__() - 10].strip()
    else:
        circunscrip = line[fin_dir+1:].strip()

    data["circunscrip"] = circunscrip
    data["direccion"] = direccion
    return data


def outSQL(archivo):
    arr_sql = []
    with open(archivo, "r", encoding="utf-8") as f:
        lines = f.readlines()
    global comunas, provincias, regiones
    comuna = comunas.index("NO DATA")
    region = regiones.index("NO DATA")
    provincia = provincias.index("NO DATA")
    sql_persona = "INSERT IGNORE INTO persona (rut, entire_name, address_servel, circuns, provincia_id, comuna_id, region_id, gender_id) values "
    c_lines = 0
    for line in lines:
        c_lines += 1
        aux_rut = re.search(r"([0-9]{1,2}\.[0-9]{3}\.[0-9]{3}-[0-9kK]{1})", line)
        #  print line
        lista = line.split()
        if "COMUNA: " in line:
            comuna = search_comuna(lista)
            region = search_region(lista)
            if comuna not in comunas:
                comunas.append(comuna)
                sql = 'INSERT IGNORE INTO comuna (idComuna, nombre) values ({}, "{}");'.format(comunas.index(comuna),
                                                                                        comuna.replace("'", " ").replace("`", " ").replace("\\", " ").replace("/", ' ').replace('"', " "))
                arr_sql.append(sql)

            if region not in regiones:
                regiones.append(region)
                sql = 'INSERT IGNORE INTO region (idRegion, nombre) values ({}, "{}");'.format(regiones.index(region),
                                                                                        region.replace("'", " ").replace("`", " ").replace("\\", " ").replace("/", ' ').replace('"', " "))
                arr_sql.append(sql)

            comuna = comunas.index(comuna)
            region = regiones.index(region)

        if "PROVINCIA" in line and not aux_rut:
            provincia = search_provincia(lista)
            if provincia not in provincias:
                provincias.append(provincia)
                sql = 'INSERT IGNORE INTO provincia (idProvincia, nombre) values ({}, "{}");'.format(
                    provincias.index(provincia), provincia.replace("'", " ").replace("`", " ").replace("\\", " ").replace("/", ' ').replace('"', " "))
                arr_sql.append(sql)

            provincia = provincias.index(provincia)

        if aux_rut:
            #  direccion = 'NO DATA'
            #  circunscrip = 'NO DATA'
            rut = aux_rut.group(0)
            rut_insert = rut.replace(".", "").split("-")[0]
            nombre = search_nombre(lista, rut).replace("'", " ").replace("`", " ").replace("\\", " ").replace("/", ' ').replace('"', " ")
            sexo = search_genero(lista, rut)
            circuns_direccion = search_circuns_direccion(line, sexo)
            if sexo in gender:
                sexo = gender.index(sexo)
            else:
                sexo = 3
            direccion = circuns_direccion["direccion"].replace("'", " ").replace("`", " ").replace("\\", " ").replace("/", ' ').replace('"', " ")
            circunscrip = circuns_direccion["circunscrip"].replace("'", " ").replace("`", " ").replace("\\", " ").replace("/", ' ').replace('"', " ")
            if len(circunscrip) > 30:
                circunscrip = ""

            #print(data)
            #  insert.insert_human(rut,nombre,sexo,direccion,comuna,provincia,region,circunscrip)
            sql_persona += " ({}, '{}', '{}', '{}', {}, {}, {}, {})".format(
                rut_insert, nombre, direccion, circunscrip, provincia, comuna, region, sexo
            )

            if len(lines)-1 == c_lines:
                sql_persona += ";"
                arr_sql.append(sql_persona)
            else:
                sql_persona += ","


    #  print(comunas)
    return arr_sql


out_file = "out.sql"
archivos = glob.glob('*.txt')
nuevo = open(out_file, 'w+')
nuevo.close()
for archivo in archivos:
    print('[+] ABRIENDO ARCHIVO ' + archivo)
    out = open(out_file, 'a')
    sql = outSQL(archivo)
    for sentencia in sql:
        out.write(sentencia + '\n')

    out.close()
